name := "gstreamer-scala"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val ScalazV = "7.1.5"
  val AkkaV = "2.4.0"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % AkkaV,
    "org.scalaz" %% "scalaz-core" % ScalazV,
    "org.scalaz" %% "scalaz-effect" % ScalazV,
    "org.scalaz" %% "scalaz-concurrent" % ScalazV,
    "org.scalatest" %% "scalatest" % "2.2.5",
    "net.java.dev.jna" % "jna" % "4.2.1"
  )
}