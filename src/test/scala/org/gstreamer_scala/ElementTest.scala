package org.gstreamer_scala

import org.gstreamer_scala.elements.{gstImpli, Element}
import org.scalatest._
import gstImpli._

/**
 * Created by ktz on 15. 11. 25.
 */
class ElementTest extends FlatSpec with Matchers {
  "Make FakeSrc And FakeSink" should "WorkWell" in {
    Gst.init()
    val bin = (Element("source", "fakesrc", Map.empty) ++ Element("sink", "fakesink", Map.empty)).
      link(src => src.mElementName == "source", dest => dest.mElementName == "sink")
    Gst.make(bin.right.get) shouldNot be (Left)
    Gst.deinit()
  }

  "Make FakeSrc And FakeSink" should "Play Well" in {
    Gst.init()
    val bin = (Element("source", "fakesrc", Map.empty) ++ Element("sink", "fakesink", Map.empty)).
      link(src => src.mElementName == "source", dest => dest.mElementName == "sink")
    val binActor = Gst.make(bin.right.get)
//    binActor.right.get ! gstPlay()
    Gst.deinit()
  }
}
