package org.gstreamer_scala

import akka.actor.{Props, ActorSystem}
import org.scalatest._

/**
 * Created by ktz on 15. 10. 27.
 */
class InitTest extends FlatSpec with Matchers{
  val system = ActorSystem("testSystem")
  "GstInitTest" should "InitNDeinit" in {
    Gst.init("")
    Gst.getVersion._1 should be (1)
    Gst.deinit()
  }

  "GstInitTest" should "IsInit==true" in {
    Gst.init("")
    Gst.isInit should be (true)
    Gst.deinit()
  }

  "GstInitTest" should "GetVersionInStringWell" in {
    Gst.init("")
    val version = Gst.getVersionString
    println(s"version : $version")
    version.isEmpty should be(false)
    Gst.deinit()
  }
}
