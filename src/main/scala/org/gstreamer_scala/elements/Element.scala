package org.gstreamer_scala.elements

import java.util.UUID
import com.sun.jna.{Pointer, Callback}
import org.gstreamer_scala.jna.Library
import scala.language.dynamics
import scalaz.effect.IO

/**
 * Created by ktz on 15. 11. 25.
 */
//ToDo gst Funtion을 각자에게 두고 BinActor는 실행을 하게 하자.
case class Caps(caps : String)
class Element(val mElementName : String, val mFactoryName : String, val mProps : Map[String, Any] = Map.empty, val signals : Map[String, Callback] = Map.empty, val mCaps : Option[Caps] = None, val mUUID : String = UUID.randomUUID().toString) extends Dynamic{
  def ++(binToAdd : Bin) : Bin = Bin(List(this) ++ binToAdd.mElements, binToAdd.mLinkInfo, UUID.randomUUID().toString)

  protected def makeRealElement(elementFactoryName : String, elementName : String)(implicit gst : Library) : Pointer = gst.gst_element_factory_make(elementFactoryName, elementName)[Pointer]
  protected def setProps(elemPtr : Pointer, propsKey : String, propValue : Any)(implicit gst : Library) : Unit = gst.g_object_set(elemPtr, propsKey, propValue)[Unit]
  protected def setCaps(elemPtr : Pointer, caps : Caps)(implicit gst : Library) : Unit = {
    val capsPtr : Pointer = gst.gst_caps_new_empty_simple(caps)[Pointer]
    gst.g_object_set(elemPtr, "cap", capsPtr)[Unit]
  }

  def convertToRealElement(elementName : String, factoryName : String, props : Map[String, Any], signals : Map[String, Callback], caps : Option[Caps])(implicit gst : Library) : IO[Pointer] = IO{
    val elemPtr = makeRealElement(elementName, factoryName)
    props.foreach(props => setProps(elemPtr, props._1, props._2))
    caps match {
      case Some(cap) => setCaps(elemPtr, cap)
      case None =>
    }
    elemPtr
  }
}

object Element {
  def apply(mElementName : String, mFactoryName : String, mProps : Map[String, Any] = Map.empty, signals : Map[String, Callback] = Map.empty, caps : Caps = null, mUUID : String = UUID.randomUUID().toString) : Element =
  if(caps == null)
    new Element(mElementName, mFactoryName, mProps, signals, None, mUUID)
  else
    new Element(mElementName, mFactoryName, mProps, signals, Some(caps), mUUID)
}