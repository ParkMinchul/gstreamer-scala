package org.gstreamer_scala.elements

import java.util.UUID

import com.sun.jna.Pointer
import org.gstreamer_scala.jna.Library
import org.gstreamer_scala.{NoSuchElementException, gstreamerException}

import scalaz.effect.IO


/**
 * Created by ktz on 15. 11. 25.
 */
class Bin private(val mElements : List[Element], val mLinkInfo : List[IO[(Library, Map[Element, Pointer]) => Boolean]] = List.empty, val mUUID : String = UUID.randomUUID().toString) {
  private def this(elements : List[Element], linkInfo : List[IO[(Library, Map[Element, Pointer]) => Boolean]]) = this(elements, linkInfo, java.util.UUID.randomUUID().toString)
  /**
   * attach Bin or Element
   * @param binToAdd : element to attach
   * @return attached Bin
   */

  def ++(binToAdd: Bin): Bin = new Bin(mElements ++ binToAdd.mElements, mLinkInfo ++ binToAdd.mLinkInfo)

  def link(src : (Element => Boolean), dest : (Element => Boolean)) : Either[gstreamerException, Bin] = createLink(src, dest) match {
    case Right(info) =>
      Right(new Bin(mElements, mLinkInfo ++ List(info), mUUID))
    case Left(gstException) =>
      Left(gstException)
  }

  private def createLink(src : (Element => Boolean), dest : (Element => Boolean)) : Either[gstreamerException, IO[(Library, Map[Element, Pointer]) => Boolean]] =
    (mElements.find(src), mElements.find(dest)) match {
      case (Some(srcElement), Some(destElement)) => Right(IO[(Library, Map[Element, Pointer]) => Boolean]{(gst : Library, elementList : Map[Element, Pointer]) =>
        gst.gst_element_link(elementList(srcElement), elementList(destElement))[Boolean]})
      case _ =>
        Left(NoSuchElementException)
    }
}

object Bin {
  def apply(mElements : List[Element], mLinkInfo : List[IO[(Library, Map[Element, Pointer]) => Boolean]], UUID : String) : Bin =
    new Bin(mElements, mLinkInfo, UUID)
  def apply(mElements : List[Element], mLinkInfo : Map[Element => Boolean, Element => Boolean]) : Either[gstreamerException, Bin] = {
    val firstBin : Either[gstreamerException, Bin] = Right(new Bin(mElements, List.empty))
    mLinkInfo.foldLeft(firstBin){(bin : Either[gstreamerException, Bin], linkInfo : (Element => Boolean, Element => Boolean)) =>
      bin match {
        case Right(rbin) =>
          rbin.link(linkInfo._1, linkInfo._2)
        case Left(gstE) => Left(gstE)
      }
    }
  }
}