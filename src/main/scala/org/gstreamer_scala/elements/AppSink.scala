package org.gstreamer_scala.elements

import java.util.UUID

import com.sun.jna.Callback

/**
 * Created by ktz on 15. 12. 27.
 */
class AppSink(mElementName : String, mProps : Map[String, Any] = Map.empty, signals : Map[String, Callback] = Map.empty, mCaps : Option[Caps] = None, mUUID : String = UUID.randomUUID().toString) extends Element(mElementName, "appsink", mProps, signals, mCaps, mUUID){

}