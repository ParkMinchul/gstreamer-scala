package org.gstreamer_scala.elements

/**
 * Created by ktz on 15. 12. 23.
 */
object gstImpli {
  implicit def elementToBin(element : Element) : Bin = Bin(List(element), Map.empty).right.get
}
