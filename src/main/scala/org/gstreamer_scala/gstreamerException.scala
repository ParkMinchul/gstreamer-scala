package org.gstreamer_scala

/**
 * Created by ktz on 15. 12. 2.
 */
trait gstreamerException extends Exception

object gstreamerException {
  def apply() : gstreamerException = new gstreamerException {}
}

case object NoSuchElementException extends gstreamerException
case object ElementInitError extends gstreamerException