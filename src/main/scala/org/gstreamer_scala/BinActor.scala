package org.gstreamer_scala

import akka.actor.{ActorLogging, Actor}
import com.sun.jna.{Callback, Pointer}
import org.gstreamer_scala.GstState.GstState
import org.gstreamer_scala.elements.{Caps, Element, Bin}
import org.gstreamer_scala.jna.Library

/**
 * Created by ktz on 15. 11. 25.
 */

case object ElementCreated
object GstState extends Enumeration {
  type GstState = Value
  val VoidPending, Null, Ready, Pause, Playing = Value
}
class BinActor(binToMake : Bin, gst : Library) extends Actor with ActorLogging{
  val gobj = Library("gobject-2.0")
  val binPtr : Pointer = gst.gst_bin_new(binToMake.mUUID)[Pointer]
  val mElements : Map[Element, Pointer] = (for{
    elem <- binToMake.mElements
    elemPtr : Pointer = ConvertToRealElement(elem)
    result = gst.gst_bin_add(binPtr, elemPtr)[Boolean]
  } yield (elem, elemPtr)).toMap

  val linkResult = for{
    link <- binToMake.mLinkInfo
    result = link.map(func => func(gst, mElements)).unsafePerformIO()
  } yield result

  if(mElements.exists(_._2 == Pointer.NULL) || linkResult.contains(false)) context.become(InitError)

  def receive = {
    case state : GstState =>
      gst.gst_element_set_state(binPtr, state.id)[Int]
      val result = gst.gst_element_get_state (binPtr, null, null, -1)[Int]
      println(result)
    case ElementCreated => sender() ! true
    case _=> sender() ! ElementInitError
  }

  def InitError : Receive = {
    case ElementCreated => sender() ! false
    case _=> sender() ! ElementInitError
  }

  protected def ConvertToRealElement(elementToConvert: Element) : Pointer = {
    val elemPtr = gst.gst_element_factory_make(elementToConvert.mFactoryName, elementToConvert.mFactoryName)[Pointer]
    elementToConvert.mProps.foreach{prop =>
      val result = gst.g_object_set(elemPtr, prop._1, prop._2)[Unit]
    }
    setCaps(elemPtr, elementToConvert.mCaps)
    gobj.g_signal_connect_data(elemPtr, "about-to-finish", playbinCallback, null, null, 0)[Int]
    elemPtr
  }

  object playbinCallback extends Callback {
    def about_to_finish(elem: Pointer, userData : Pointer) : Unit = {
      println("about to finish!!!")
    }
  }

  protected def setCaps(elemPtr : Pointer, caps : Option[Caps]) : Unit = {
    if(caps.isDefined){
      val capsPtr : Pointer = gst.gst_caps_new_empty_simple(caps.get.caps)[Pointer]
      gst.g_object_set(elemPtr, "cap", capsPtr)[Unit]
    }
  }
}