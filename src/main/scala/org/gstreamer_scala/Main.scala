package org.gstreamer_scala

import akka.actor.{Props, Actor, ActorSystem}
import org.gstreamer_scala.elements.{gstImpli, Element, Bin}
import gstImpli._
import org.gstreamer_scala.elements.{Element, Bin}

/**
 * Created by ktz on 15. 11. 29.
 */
object Main extends App{
  Gst.init()
//  val bin = (Element("source", "fakesrc", Map.empty) ++ Element("sink", "fakesink", Map.empty)).
//    link(src => src.mElementName == "source", dest => dest.mElementName == "sink")
//  val binActor = Gst.make(bin.right.get)
//  binActor.right.get ! GstState.Playing
  val playbin = Bin(List(Element("playBin", "playbin", Map("uri" -> "file:///home/ktz/out.mp3"))), Map.empty)
  val binActor = Gst.make(playbin.right.get)
  binActor.right.get ! GstState.Playing
}
