package org.gstreamer_scala.lowlevel.Struct

//import java.util

import com.sun.jna.{Pointer, Structure}

//import scala.collection.mutable.ListBuffer

/**
 * Created by ktz on 15. 12. 27.
 */
class GstMiniObject(ptr : Pointer) extends Structure{
  if(ptr != null) {
    useMemory(ptr)
    read()
  }

  def this() {
    this(null)
  }
  var gstType : Long = _
  var lockstate : Int = _
  var flag : Int = _
  var copyFn : Pointer = _
  var disposeFn : Pointer = _
  var freeFn : Pointer = _

//  import scala.collection.JavaConversions._
//  override def getFieldOrder: util.List[_] = ListBuffer("type", "refcount", "lockstate", "flags",
//    "copyFn", "disposeFn", "freeFn")
}
