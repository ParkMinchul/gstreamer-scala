package org.gstreamer_scala.lowlevel.Struct

import com.sun.jna.{Pointer, Structure}

/**
 * Created by ktz on 15. 12. 27.
 */
class GstBuffer(ptr : Pointer) extends Structure{
  if(ptr != null) {
    useMemory(ptr)
    read()
  }

  def this() {
    this(null)
  }
  var mini_object : GstMiniObject = _
  var pts : Long = _
  var dts : Long = _
  var duration : Long = _
  var offset : Long = _
  var offset_end : Long = _

}
