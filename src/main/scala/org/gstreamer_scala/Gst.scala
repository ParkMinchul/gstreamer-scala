package org.gstreamer_scala

import akka.actor.{Props, ActorRef, ActorSystem}
import com.sun.jna.ptr.IntByReference
import akka.pattern.ask
import org.gstreamer_scala.elements.Bin
import org.gstreamer_scala.jna.Library

import scala.concurrent.Await
import scala.concurrent.duration._
import scalaz.Scalaz._
/**
 * Created by ktz on 15. 10. 26.
 */

object Gst {
  private val gst = Library("gstreamer-1.0")
  private var gstActorSystem : ActorSystem = ActorSystem("gstActorSystem")
  /**
   * Gets the version of gstreamer currently in use.
   *
   * @return the version of gstreamer
   */

  def getVersion : (Int, Int, Int, Int) = {
    val major : IntByReference = new IntByReference()
    val minor : IntByReference = new IntByReference()
    val micro : IntByReference = new IntByReference()
    val nano : IntByReference = new IntByReference()
    gst.gst_version(major, minor, micro, nano)[Unit]
    (major.getValue, minor.getValue, micro.getValue, nano.getValue)
  }

  def getVersionString : String = gst.gst_version_string()[String]

  def init(args : Array[String]) : Unit = {
    gst.gst_init(new IntByReference(args.length), args)[Unit]
  }

  def init(args : String) : Unit = init(args.split(" "))

  def init() : Unit = init("")

  def deinit() : Unit = gst.gst_deinit()[Unit]

  def isInit : Boolean = gst.gst_is_initialized()[Boolean]

  def setActorSystem(actorSystem: ActorSystem) : Unit = {
    gstActorSystem.terminate()
    gstActorSystem = actorSystem
  }

  def make(binToMake : Bin) : Either[gstreamerException, ActorRef] = {
    val binActor = gstActorSystem.actorOf(Props(new BinActor(binToMake, gst)))
    val result : Either[gstreamerException, ActorRef] = if(Await.result(binActor.ask(ElementCreated)(10 second), 10 second).asInstanceOf[Boolean]) Right(binActor)
    else Left(gstreamerException())
    result
  }
}